import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal({ name, source }) {
  const winnerImageAttributes = {
    src: source,
    title: name,
    alt: name,
  };
  const modalWinnerBody = createElement({
    tagName: 'div',
    className: 'modal-winner__img',
    child: createElement({
      tagName: 'img',
      attributes: winnerImageAttributes,
    }),
  });

  showModal({
    title: `${name} won!`,
    bodyElement: modalWinnerBody,
    onClose: () => {
      document.querySelector('#root').innerHTML = '';
      new App();
    },
  });
}
