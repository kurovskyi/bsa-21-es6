import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const { name, source, health, attack, defense } = fighter;

    const fighterNameElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___fighter-name',
      child: createElement({
        tagName: 'p',
      }),
    });
    fighterNameElement.querySelector('p').innerHTML = name;

    const fighterImageAttributes = {
      src: source,
      title: name,
      alt: name,
    };
    const fighterImageElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___fighter-img',
      child: createElement({
        tagName: 'img',
        attributes: fighterImageAttributes,
      }),
    });

    const fighterStatsElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___fighter-stats',
      child: createElement({
        tagName: 'p',
      }),
    });
    fighterStatsElement.querySelector('p').innerHTML = `XP: ${health} / A: ${attack} / D: ${defense}`;

    fighterElement.append(fighterNameElement, fighterImageElement, fighterStatsElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
