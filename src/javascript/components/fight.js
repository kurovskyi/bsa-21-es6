import { controls } from '../../constants/controls';

export function generateExtendedFighterState(fighter, healthBarElement) {
  const extendedFighterState = {
    isDefending: false,
    canUseCombination: true,
  };

  fighter = {
    ...fighter,
    ...extendedFighterState,
    maxHealth: fighter.health,
    healthBarElement,
  };

  return fighter;
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const [firstFighterHealthBar, secondFighterHealthBar] = document.getElementsByClassName('arena___health-bar');

    firstFighter = generateExtendedFighterState(firstFighter, firstFighterHealthBar);
    secondFighter = generateExtendedFighterState(secondFighter, secondFighterHealthBar);

    const activeKeysPressed = new Set();

    document.addEventListener('keydown', ({ code: keyCode }) => {
      if (
        controls.PlayerOneCriticalHitCombination.includes(keyCode) ||
        controls.PlayerTwoCriticalHitCombination.includes(keyCode)
      ) {
        activeKeysPressed.add(keyCode);
      }

      if (
        controls.PlayerOneCriticalHitCombination.every((key) => activeKeysPressed.has(key)) &&
        firstFighter.canUseCombination
      )
        processAttack(firstFighter, secondFighter, true);
      else if (
        controls.PlayerTwoCriticalHitCombination.every((key) => activeKeysPressed.has(key)) &&
        secondFighter.canUseCombination
      )
        processAttack(secondFighter, firstFighter, true);
      else
        switch (keyCode) {
          case controls.PlayerOneAttack: {
            processAttack(firstFighter, secondFighter);
            break;
          }
          case controls.PlayerTwoAttack: {
            processAttack(secondFighter, firstFighter);
            break;
          }
          case controls.PlayerOneBlock: {
            firstFighter.isDefending = true;
            break;
          }
          case controls.PlayerTwoBlock: {
            secondFighter.isDefending = true;
            break;
          }
        }

      if (firstFighter.health <= 0) return resolve(secondFighter);
      else if (secondFighter.health <= 0) return resolve(firstFighter);
    });

    document.addEventListener('keyup', ({ code: keyCode }) => {
      if (activeKeysPressed.has(keyCode)) activeKeysPressed.delete(keyCode);

      switch (keyCode) {
        case controls.PlayerOneBlock: {
          return (firstFighter.isDefending = false);
        }
        case controls.PlayerTwoBlock: {
          return (secondFighter.isDefending = false);
        }
      }
    });
  });
}

export function processAttack(attacker, defender, isCombination) {
  if (attacker.isDefending) return;

  const damage = isCombination ? attacker.attack * 2 : getDamage(attacker, defender);

  if (defender.health - damage < 0) defender.health = 0;
  else defender.health -= damage;

  const defenderHealthPercent = (defender.health * 100) / defender.maxHealth;
  defender.healthBarElement.style.width = `${defenderHealthPercent}%`;

  if (isCombination) {
    attacker.canUseCombination = false;
    setTimeout(() => {
      attacker.canUseCombination = true;
    }, 10000);
  }
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker);

  if (defender.isDefending) damage -= getBlockPower(defender);
  if (damage < 0) damage = 0;

  return damage;
}

export function getHitPower({ attack }) {
  const criticalHitChance = Math.random() + 1;
  const hitPower = attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower({ defense }) {
  const dodgeChance = Math.random() + 1;
  const blockPower = defense * dodgeChance;

  return blockPower;
}
